package br.com.bs.atendimento.common;

import org.apache.logging.log4j.util.Strings;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public abstract class DateUtil {

    public static LocalDateTime stringToLocalDateTime(String date, String format) {
        if (Strings.isNotEmpty(date) && Strings.isNotEmpty(format)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.parse(date, formatter);
        }
        return null;
    }

    public static LocalDate stringToLocalDate(String date, String format) {
        if (Strings.isNotEmpty(date) && Strings.isNotEmpty(format)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDate.parse(date, formatter);
        }
        return null;
    }

    public static String formatarDataHoraMinutoSegundos(LocalDateTime localDateTime) {
        if (Objects.nonNull(localDateTime)) {
            return localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        }
        return null;
    }
}
