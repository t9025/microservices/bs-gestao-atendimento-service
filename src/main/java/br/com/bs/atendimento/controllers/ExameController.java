package br.com.bs.atendimento.controllers;

import api.ExamesApi;
import br.com.bs.atendimento.services.atendimento.ExameService;
import lombok.RequiredArgsConstructor;
import model.ExameDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ExameController implements ExamesApi {

    private final ExameService exameService;

    @Override
    public  ResponseEntity<Void> apagarExame(Integer id) {
        this.exameService.apagarExame(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> atualizarExame(Integer id, String xStatus) {
        exameService.atualizarExame(id, xStatus);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ExameDTO> obterExame(Integer id) {
        return new ResponseEntity<>(exameService.obterExame(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ExameDTO>> obterTodosExames() {
        return new ResponseEntity<>(exameService.obterTodosExames(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ExameDTO>> pesquisarExames(String xAssociado, String xData, Boolean xStatus, String xPrestador, String xConveniado, String xTipo) {
        return new ResponseEntity<>(exameService.pesquisarExame(xAssociado, xData, xStatus, xPrestador, xConveniado, xTipo), HttpStatus.OK);
    }

}
