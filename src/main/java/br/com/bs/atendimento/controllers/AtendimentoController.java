package br.com.bs.atendimento.controllers;

import api.AtendimentosApi;
import br.com.bs.atendimento.services.atendimento.AtendimentoService;
import lombok.RequiredArgsConstructor;
import model.AtendimentoDTO;
import model.AtualizarAtendimentoDTO;
import model.RetornoAtendimentoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AtendimentoController implements AtendimentosApi {

    private final AtendimentoService atendimentoService;

    @Override
    public ResponseEntity<RetornoAtendimentoDTO> obterAtendimento(Integer id) {
        return new ResponseEntity<>(this.atendimentoService.obterAtendimento(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<RetornoAtendimentoDTO>> obterTodosAtendimentos() {
        return new ResponseEntity<>(this.atendimentoService.obterTodosAtendimento(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<RetornoAtendimentoDTO> novoAtendimento(AtendimentoDTO atendimentoDTO) {
        return new ResponseEntity<>(this.atendimentoService.novoAtendimento(atendimentoDTO), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<RetornoAtendimentoDTO> atualizarAtendimento(Integer id, AtualizarAtendimentoDTO atualizarAtendimentoDTO) {
        return new ResponseEntity<>(this.atendimentoService.atualizarAtendimento(id, atualizarAtendimentoDTO), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> apagarAtendimento(Integer id) {
        this.atendimentoService.apagarAtendimento(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<RetornoAtendimentoDTO>> pesquisarAtendimento(String xAssociado, String xData, Boolean xStatus, String xPrestador, String xConveniado) {

        return new ResponseEntity<>(this.atendimentoService.pesquisarAtendimento(xAssociado, xData, xStatus, xPrestador, xConveniado),
                HttpStatus.OK);

    }


}
