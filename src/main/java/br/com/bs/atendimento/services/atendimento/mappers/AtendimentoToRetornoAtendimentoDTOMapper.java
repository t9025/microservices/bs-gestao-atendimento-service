package br.com.bs.atendimento.services.atendimento.mappers;

import br.com.bs.atendimento.common.DateUtil;
import br.com.bs.atendimento.entities.Atendimento;
import model.ExameDTO;
import model.RetornoAtendimentoDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;

@Component
public class AtendimentoToRetornoAtendimentoDTOMapper {

    public RetornoAtendimentoDTO mapAtoB(Atendimento source) {
        RetornoAtendimentoDTO target = new RetornoAtendimentoDTO();
        target.setId(source.getId());
        target.setPrestador(source.getPrestador());
        target.setAssociado(source.getAssociado());
        target.setConveniado(source.getConveniado());
        target.setHorario(DateUtil.formatarDataHoraMinutoSegundos(source.getDataAgendamento()));
        target.setConfirmado(source.getConfirmado());
        target.setObservacoes(source.getObservacoes());

        if (!CollectionUtils.isEmpty(source.getExames())) {
            var exames = new ArrayList<ExameDTO>();
            source.getExames().forEach(exame -> {
                var targetExame = new ExameDTO();
                targetExame.setId(exame.getId());
                targetExame.setPrestador(exame.getPrestador());
                targetExame.setAssociado(exame.getAssociado());
                targetExame.setConveniado(exame.getConveniado());
                targetExame.setHorario(DateUtil.formatarDataHoraMinutoSegundos(exame.getDataAgendamento()));
                targetExame.setConfirmado(exame.getConfirmado());
                targetExame.setObservacoes(exame.getObservacoes());
                targetExame.setTipo(exame.getTipo());
                exames.add(targetExame);
            });
            target.setExames(exames);
        }

        return target;
    }
}
