package br.com.bs.atendimento.services.atendimento.mappers;

import br.com.bs.atendimento.common.DateUtil;
import br.com.bs.atendimento.entities.Exame;
import model.ExameDTO;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ExameDTOToExameMapper {

    public Exame mapExameDtoToExame(ExameDTO source, Map<String, String> contextMapper) {
        var target = Exame.builder().build();
        target.setPrestador(source.getPrestador());
        target.setAssociado(source.getAssociado());
        target.setConveniado(source.getConveniado());
        target.setCriadoPor(contextMapper.get("sessionUser"));
        target.setDataAgendamento(DateUtil.stringToLocalDateTime(source.getHorario(), "dd/MM/yyyy HH:mm:ss"));
        target.setTipo(source.getTipo());
        return target;
    }
}
