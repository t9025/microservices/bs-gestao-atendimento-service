package br.com.bs.atendimento.services.atendimento;

import model.AtendimentoDTO;
import model.AtualizarAtendimentoDTO;
import model.RetornoAtendimentoDTO;

import java.util.List;

public interface AtendimentoService {

    RetornoAtendimentoDTO novoAtendimento(AtendimentoDTO atendimentoDTO);

    RetornoAtendimentoDTO obterAtendimento(Integer id);

    List<RetornoAtendimentoDTO> obterTodosAtendimento();

    RetornoAtendimentoDTO atualizarAtendimento(Integer id, AtualizarAtendimentoDTO atualizarAtendimentoDTO);

    void apagarAtendimento(Integer id);

    List<RetornoAtendimentoDTO> pesquisarAtendimento(String xAssociado, String xData, Boolean xStatus, String xPrestador, String xConveniado);
}
