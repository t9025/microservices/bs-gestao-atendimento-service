package br.com.bs.atendimento.services.atendimento;

import br.com.bs.atendimento.common.DateUtil;
import br.com.bs.atendimento.exception.NotFoundException;
import br.com.bs.atendimento.repositories.AtendimentoRepository;
import br.com.bs.atendimento.services.BaseService;
import br.com.bs.atendimento.services.atendimento.mappers.AtendimentoDTOToAtendimentoMapper;
import br.com.bs.atendimento.services.atendimento.mappers.AtendimentoToRetornoAtendimentoDTOMapper;
import br.com.bs.atendimento.services.atendimento.mappers.ExameDTOToExameMapper;
import lombok.RequiredArgsConstructor;
import model.AtendimentoDTO;
import model.AtualizarAtendimentoDTO;
import model.RetornoAtendimentoDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AtendimentoServiceImpl extends BaseService implements AtendimentoService {

    private final AtendimentoRepository atendimentoRepository;

    private final AtendimentoDTOToAtendimentoMapper atendimentoDTOToAtendimentoMapper;
    private final AtendimentoToRetornoAtendimentoDTOMapper atendimentoToRetornoAtendimentoDTOMapper;
    private final ExameDTOToExameMapper exameDTOToExameMapper;

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO')")
    public RetornoAtendimentoDTO novoAtendimento(AtendimentoDTO atendimentoDTO) {
        var atendimento = this.atendimentoDTOToAtendimentoMapper.mapAtoB(atendimentoDTO,
                Map.of("sessionUser", super.getSessionUsername()));
        return this.atendimentoToRetornoAtendimentoDTOMapper.mapAtoB(this.atendimentoRepository.save(atendimento));
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO') or hasAnyAuthority('ROLE_MEDICO')")
    public RetornoAtendimentoDTO obterAtendimento(Integer id) {
        var optAtendimento = this.atendimentoRepository.findById(id);
        return optAtendimento.map(this.atendimentoToRetornoAtendimentoDTOMapper::mapAtoB)
                .orElseThrow(() -> new NotFoundException(super.message.get("atendimento.nao.encontrado")));

    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO')")
    public List<RetornoAtendimentoDTO> obterTodosAtendimento() {
        var atendimentos = this.atendimentoRepository.findAll();

        if (CollectionUtils.isEmpty(atendimentos)) {
            throw new NotFoundException(super.message.get("nenhum.atendimento.encontrado"));
        }

        var retornoAtendimento = new ArrayList<RetornoAtendimentoDTO>(atendimentos.size());
        atendimentos.forEach(atendimento -> retornoAtendimento.add(this.atendimentoToRetornoAtendimentoDTOMapper.mapAtoB(atendimento)));
        return retornoAtendimento;
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO') or hasAnyAuthority('ROLE_MEDICO')")
    public RetornoAtendimentoDTO atualizarAtendimento(Integer id, AtualizarAtendimentoDTO atualizarAtendimentoDTO) {
        var atendimento = this.atendimentoRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(super.message.get("atendimento.nao.encontrado")));

        atendimento.setPrestador(atualizarAtendimentoDTO.getPrestador());
        atendimento.setConveniado(atualizarAtendimentoDTO.getConveniado());
        atendimento.setDataAgendamento(DateUtil.stringToLocalDateTime(
                atualizarAtendimentoDTO.getHorario(),
                "dd/MM/yyyy HH:mm:ss"));
        atendimento.setObservacoes(atualizarAtendimentoDTO.getObservacoes());
        atendimento.setConfirmado(atualizarAtendimentoDTO.getConfirmado());
        atendimento.setAtualizadoPor(super.getSessionUsername());

        if(!CollectionUtils.isEmpty(atualizarAtendimentoDTO.getExames())){
            if (CollectionUtils.isEmpty(atendimento.getExames())) {
                atendimento.setExames(new ArrayList<>());
            }
            atualizarAtendimentoDTO.getExames().forEach(e -> {
                var exame = exameDTOToExameMapper.mapExameDtoToExame(e, Map.of("sessionUser", super.getSessionUsername()));
                atendimento.getExames().add(exame);
            });
        }
        return this.atendimentoToRetornoAtendimentoDTOMapper.mapAtoB(this.atendimentoRepository.save(atendimento));
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO')")
    public void apagarAtendimento(Integer id) {
        var atendimento = this.atendimentoRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(super.message.get("atendimento.nao.encontrado")));
        this.atendimentoRepository.delete(atendimento);
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO')")
    public List<RetornoAtendimentoDTO> pesquisarAtendimento(String xAssociado, String xData, Boolean xStatus, String xPrestador, String xConveniado) {
        var localDateDataInicio = DateUtil.stringToLocalDate(xData, "dd/MM/yyyy");
        LocalDateTime dataInicio = null;
        LocalDateTime dataFim = null;

        if (Objects.nonNull(localDateDataInicio)) {
            dataInicio = localDateDataInicio.atTime(0, 0, 0);
            dataFim = localDateDataInicio.plusDays(1).atTime(23, 59, 59);
        }

        var resultados = this.atendimentoRepository.pesquisar(xAssociado, xPrestador, xConveniado, dataInicio, dataFim, xStatus);

        if (CollectionUtils.isEmpty(resultados)) {
            throw new NotFoundException(super.message.get("nenhum.atendimento.encontrado"));
        }

        var atendimentos = new ArrayList<RetornoAtendimentoDTO>();
        resultados.forEach(resultado -> atendimentos.add(this.atendimentoToRetornoAtendimentoDTOMapper.mapAtoB(resultado)));

        return atendimentos;
    }
}
