package br.com.bs.atendimento.services.atendimento;

import model.ExameDTO;

import java.util.List;

public interface ExameService {

    ExameDTO obterExame(Integer id);

    List<ExameDTO> obterTodosExames();

    List<ExameDTO> pesquisarExame(String xAssociado, String xData, Boolean xStatus, String xPrestador, String xConveniado, String xTipo);

    void atualizarExame(Integer id, String xStatus);

    void apagarExame(Integer id);
}
