package br.com.bs.atendimento.services.atendimento.mappers;

import br.com.bs.atendimento.common.DateUtil;
import br.com.bs.atendimento.entities.Atendimento;
import model.AtendimentoDTO;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AtendimentoDTOToAtendimentoMapper {

    public Atendimento mapAtoB(AtendimentoDTO source, Map<String, String> contextMapper) {
        var target = Atendimento.builder().build();
        target.setPrestador(source.getPrestador());
        target.setAssociado(source.getAssociado());
        target.setConveniado(source.getConveniado());
        target.setCriadoPor(contextMapper.get("sessionUser"));
        target.setDataAgendamento(DateUtil.stringToLocalDateTime(source.getHorario(), "dd/MM/yyyy HH:mm:ss"));
        return target;
    }
}
