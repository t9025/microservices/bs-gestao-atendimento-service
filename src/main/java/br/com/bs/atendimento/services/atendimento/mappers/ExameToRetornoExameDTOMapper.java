package br.com.bs.atendimento.services.atendimento.mappers;

import br.com.bs.atendimento.common.DateUtil;
import br.com.bs.atendimento.entities.Exame;
import model.ExameDTO;
import org.springframework.stereotype.Component;

@Component
public class ExameToRetornoExameDTOMapper {

    public ExameDTO mapExameToExameDTO(Exame source) {
        ExameDTO target = new ExameDTO();
        target.setId(source.getId());
        target.setPrestador(source.getPrestador());
        target.setAssociado(source.getAssociado());
        target.setConveniado(source.getConveniado());
        target.setHorario(DateUtil.formatarDataHoraMinutoSegundos(source.getDataAgendamento()));
        target.setConfirmado(source.getConfirmado());
        target.setTipo(source.getTipo());
        return target;
    }
}
