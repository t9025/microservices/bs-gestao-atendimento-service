package br.com.bs.atendimento.services.atendimento;

import br.com.bs.atendimento.common.DateUtil;
import br.com.bs.atendimento.exception.NotFoundException;
import br.com.bs.atendimento.exception.UnprocessableEntityException;
import br.com.bs.atendimento.repositories.ExameRepository;
import br.com.bs.atendimento.services.BaseService;
import br.com.bs.atendimento.services.atendimento.mappers.ExameToRetornoExameDTOMapper;
import lombok.RequiredArgsConstructor;
import model.ExameDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ExameServiceImpl extends BaseService implements ExameService {

    private final ExameRepository exameRepository;
    private final ExameToRetornoExameDTOMapper exameToRetornoExameDTOMapper;

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO') or hasAnyAuthority('ROLE_MEDICO')")
    public ExameDTO obterExame(Integer id) {
        var exame = exameRepository.findById(id);
        return exame.map(this.exameToRetornoExameDTOMapper::mapExameToExameDTO)
                .orElseThrow(() -> new NotFoundException(super.message.get("exame.nao.encontrado")));
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO') or hasAnyAuthority('ROLE_MEDICO')")
    public List<ExameDTO> obterTodosExames() {
        var exames = exameRepository.findAll();

        if (CollectionUtils.isEmpty(exames)) {
            throw new NotFoundException(super.message.get("nenhum.exame.encontrado"));
        }

        var retornoExames = new ArrayList<ExameDTO>();
        exames.forEach(e -> retornoExames.add(this.exameToRetornoExameDTOMapper.mapExameToExameDTO(e)));
        return retornoExames;
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO') or hasAnyAuthority('ROLE_MEDICO')")
    public List<ExameDTO> pesquisarExame(String xAssociado, String xData, Boolean xStatus, String xPrestador, String xConveniado, String xTipo) {
        var localDateDataInicio = DateUtil.stringToLocalDate(xData, "dd/MM/yyyy");
        LocalDateTime dataInicio = null;
        LocalDateTime dataFim = null;

        if (Objects.nonNull(localDateDataInicio)) {
            dataInicio = localDateDataInicio.atTime(0, 0, 0);
            dataFim = localDateDataInicio.plusDays(1).atTime(23, 59, 59);
        }

        var resultados = exameRepository.pesquisar(xAssociado, xPrestador, xConveniado, dataInicio, dataFim, xStatus, xTipo);

        if (CollectionUtils.isEmpty(resultados)) {
            throw new NotFoundException(super.message.get("nenhum.exame.encontrado"));
        }

        var exames = new ArrayList<ExameDTO>();
        resultados.forEach(resultado -> exames.add(this.exameToRetornoExameDTOMapper.mapExameToExameDTO(resultado)));

        return exames;
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO')")
    public void atualizarExame(Integer id, String xStatus) {
        var exame = exameRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(super.message.get("exame.nao.encontrado")));

        exame.setConfirmado("AUTORIZADO".equals(xStatus));
        exame.setAtualizadoPor(super.getSessionUsername());

        exameRepository.save(exame);
    }

    @Override
    @PreAuthorize("hasAuthority('ROLE_ATENDIMENTO')")
    public void apagarExame(Integer id) {
        var exame = exameRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(super.message.get("exame.nao.encontrado")));

        if (exame.getConfirmado() != null && exame.getConfirmado()) {
            throw new UnprocessableEntityException(super.message.get("erro.exclusao.exame.autorizado"));
        }
        exameRepository.delete(exame);
    }
}
