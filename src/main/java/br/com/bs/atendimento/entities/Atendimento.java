package br.com.bs.atendimento.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ate_atendimento")
public class Atendimento {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ate_id", nullable = false)
    private Integer id;

    @Column(name = "ate_prestador", nullable = false)
    private String prestador;

    @Column(name = "ate_conveniado", nullable = false)
    private String conveniado;

    @Column(name = "ate_associado", nullable = false)
    private String associado;

    @Column(name = "ate_observacoes", length = 1000)
    private String observacoes;

    @Column(name = "ate_confirmado")
    private Boolean confirmado;

    @Column(name = "ate_data_agendamento", nullable = false)
    private LocalDateTime dataAgendamento;

    @Column(name = "ate_data_criacao", nullable = false)
    private LocalDateTime dataCriacao;

    @Column(name = "ate_criado_por", nullable = false)
    private String criadoPor;

    @Column(name = "ate_data_atualizacao")
    private LocalDateTime dataAtualizacao;

    @Column(name = "ate_atualizado_por")
    private String atualizadoPor;

    @OneToMany(cascade = {PERSIST, MERGE, REMOVE}, mappedBy = "atendimento")
    private List<Exame> exames;

    @PrePersist
    private void prePersist() {
        this.setDataCriacao(LocalDateTime.now());
        this.setConfirmado(Boolean.FALSE);
    }

    @PreUpdate
    private void perUpdate() {
        this.setDataAtualizacao(LocalDateTime.now());
    }

}
