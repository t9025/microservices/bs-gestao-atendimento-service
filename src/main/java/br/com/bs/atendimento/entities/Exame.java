package br.com.bs.atendimento.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "aex_exame")
public class Exame {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "aex_id", nullable = false)
    private Integer id;

    @Column(name = "ate_id", nullable = false)
    private Integer idAtendimento;

    @Column(name = "aex_prestador", nullable = false)
    private String prestador;

    @Column(name = "aex_conveniado", nullable = false)
    private String conveniado;

    @Column(name = "aex_associado", nullable = false)
    private String associado;

    @Column(name = "aex_observacoes", length = 1000)
    private String observacoes;

    @Column(name = "aex_confirmado")
    private Boolean confirmado;

    @Column(name = "aex_tipo", nullable = false)
    private String tipo;

    @Column(name = "aex_data_agendamento", nullable = false)
    private LocalDateTime dataAgendamento;

    @Column(name = "aex_data_criacao", nullable = false)
    private LocalDateTime dataCriacao;

    @Column(name = "aex_criado_por", nullable = false)
    private String criadoPor;

    @Column(name = "aex_data_atualizacao")
    private LocalDateTime dataAtualizacao;

    @Column(name = "aex_atualizado_por")
    private String atualizadoPor;

    @ManyToOne()
    @JoinColumn(name = "ate_id", insertable = false, updatable = false)
    private Atendimento atendimento;

    @PrePersist
    private void prePersist() {
        this.setDataCriacao(LocalDateTime.now());
        this.setConfirmado(Boolean.FALSE);
    }

    @PreUpdate
    private void perUpdate() {
        this.setDataAtualizacao(LocalDateTime.now());
    }

}
