package br.com.bs.atendimento.repositories.custom;

import br.com.bs.atendimento.entities.Exame;

import java.time.LocalDateTime;
import java.util.List;

public interface ExameRepositoryCustom {

    List<Exame> pesquisar(String associado,
                          String prestador,
                          String conveniado,
                          LocalDateTime dataAgendamentoInicio,
                          LocalDateTime dataAgendamentoFim,
                          Boolean confirmado,
                          String tipo);
}
