package br.com.bs.atendimento.repositories.custom;

import br.com.bs.atendimento.entities.Atendimento;

import java.time.LocalDateTime;
import java.util.List;

public interface AtendimentoRepositoryCustom {

    List<Atendimento> pesquisar(String associado,
                                String prestador,
                                String conveniado,
                                LocalDateTime dataAgendamentoInicio,
                                LocalDateTime dataAgendamentoFim,
                                Boolean confirmado);
}
