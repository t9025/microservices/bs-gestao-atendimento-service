package br.com.bs.atendimento.repositories.impl;

import br.com.bs.atendimento.entities.Exame;
import br.com.bs.atendimento.repositories.custom.ExameRepositoryCustom;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.time.LocalDateTime;
import java.util.List;

import static br.com.bs.atendimento.entities.QExame.exame;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class ExameRepositoryImpl extends QuerydslRepositorySupport implements ExameRepositoryCustom {

    public ExameRepositoryImpl() {
        super(Exame.class);
    }

    @Override
    public List<Exame> pesquisar(String associado,
                                 String prestador,
                                 String conveniado,
                                 LocalDateTime dataAgendamentoInicio,
                                 LocalDateTime dataAgendamentoFim,
                                 Boolean confirmado,
                                 String tipo) {
        BooleanBuilder conditions = new BooleanBuilder();
        JPQLQuery<Exame> query = from(exame);

        if (isNotBlank(associado)) {
            conditions.and(exame.associado.likeIgnoreCase("%" + associado + "%"));
        }
        if (isNotBlank(prestador)) {
            conditions.and(exame.prestador.likeIgnoreCase("%" + prestador + "%"));
        }
        if (isNotBlank(conveniado)) {
            conditions.and(exame.conveniado.likeIgnoreCase("%" + conveniado + "%"));
        }
        if (!isNull(dataAgendamentoInicio)) {
            conditions.and(exame.dataAgendamento.between(dataAgendamentoInicio, dataAgendamentoFim));
        }

        if (!isNull(confirmado)) {
            conditions.and(exame.confirmado.eq(confirmado));
        }
        if (isNotBlank(tipo)) {
            conditions.and(exame.tipo.eq(tipo));
        }
        return query.where(conditions).fetch();
    }
}
