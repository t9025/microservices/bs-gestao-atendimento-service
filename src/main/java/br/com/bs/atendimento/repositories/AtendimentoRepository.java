package br.com.bs.atendimento.repositories;

import br.com.bs.atendimento.entities.Atendimento;
import br.com.bs.atendimento.repositories.custom.AtendimentoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtendimentoRepository extends AtendimentoRepositoryCustom, JpaRepository<Atendimento, Integer> {
}
