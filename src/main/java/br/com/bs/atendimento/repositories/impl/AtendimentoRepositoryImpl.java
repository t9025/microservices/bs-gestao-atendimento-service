package br.com.bs.atendimento.repositories.impl;

import br.com.bs.atendimento.entities.Atendimento;
import br.com.bs.atendimento.repositories.custom.AtendimentoRepositoryCustom;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.time.LocalDateTime;
import java.util.List;

import static br.com.bs.atendimento.entities.QAtendimento.atendimento;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class AtendimentoRepositoryImpl extends QuerydslRepositorySupport implements AtendimentoRepositoryCustom {

    public AtendimentoRepositoryImpl() {
        super(Atendimento.class);
    }

    @Override
    public List<Atendimento> pesquisar(String associado,
                                       String prestador,
                                       String conveniado,
                                       LocalDateTime dataAgendamentoInicio,
                                       LocalDateTime dataAgendamentoFim,
                                       Boolean confirmado) {
        BooleanBuilder conditions = new BooleanBuilder();
        JPQLQuery<Atendimento> query = from(atendimento);

        if (isNotBlank(associado)) {
            conditions.and(atendimento.associado.likeIgnoreCase("%" + associado + "%"));
        }
        if (isNotBlank(prestador)) {
            conditions.and(atendimento.prestador.likeIgnoreCase("%" + prestador + "%"));
        }
        if (isNotBlank(conveniado)) {
            conditions.and(atendimento.conveniado.likeIgnoreCase("%" + conveniado + "%"));
        }
        if (!isNull(dataAgendamentoInicio)) {
            conditions.and(atendimento.dataAgendamento.between(dataAgendamentoInicio, dataAgendamentoFim));
        }
        if (!isNull(confirmado)) {
            conditions.and(atendimento.confirmado.eq(confirmado));
        }
        return query.where(conditions).fetch();
    }
}
