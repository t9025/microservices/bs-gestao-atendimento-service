package br.com.bs.atendimento.repositories;

import br.com.bs.atendimento.entities.Exame;
import br.com.bs.atendimento.repositories.custom.ExameRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExameRepository extends ExameRepositoryCustom, JpaRepository<Exame, Integer> {
}
