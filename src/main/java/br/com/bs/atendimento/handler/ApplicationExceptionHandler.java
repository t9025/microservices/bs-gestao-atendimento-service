package br.com.bs.atendimento.handler;

import br.com.bs.atendimento.component.MessageSourceService;
import br.com.bs.atendimento.exception.BadRequestException;
import br.com.bs.atendimento.exception.ForbiddenException;
import br.com.bs.atendimento.exception.InternalServerErrorException;
import br.com.bs.atendimento.exception.NoContentException;
import br.com.bs.atendimento.exception.NotFoundException;
import br.com.bs.atendimento.exception.PreconditionFailedException;
import br.com.bs.atendimento.exception.UnauthorizedException;
import br.com.bs.atendimento.exception.UnprocessableEntityException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import model.ErrorDTO;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;


@ControllerAdvice
public class ApplicationExceptionHandler {
    private static final Logger LOGGER_TRACE = LoggerFactory.getLogger(ApplicationExceptionHandler.class);
    private static final Logger LOGGER_ERROR = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

    private final MessageSourceService messageSourceService;

    @Autowired
    public ApplicationExceptionHandler(final MessageSourceService messageSourceService) {
        this.messageSourceService = messageSourceService;
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, InvalidFormatException.class})
    public ResponseEntity<List<ErrorDTO>> catchMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        final List<ErrorDTO> errors = createErrors(exception.getBindingResult());
        LOGGER_TRACE.debug(getClass().getName(), exception);
        return ResponseEntity.status(BAD_REQUEST).body(errors);
    }

    private List<ErrorDTO> createErrors(final BindingResult bindingResult) {
        final List<ErrorDTO> errors = new ArrayList<>(bindingResult.getErrorCount());
        bindingResult.getFieldErrors().forEach(fieldError -> {
            String clientMessage = messageSourceService.get(fieldError);
            errors.add(new ErrorDTO().message(clientMessage).field(fieldError.getField()));
        });
        return errors;
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorDTO> catchBadRequestException(BadRequestException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, BAD_REQUEST);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<ErrorDTO> catchInternalServerErrorException(InternalServerErrorException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NoContentException.class)
    public ResponseEntity<ErrorDTO> catchNoContentException(NoContentException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, NO_CONTENT);
    }

    @ExceptionHandler(UnprocessableEntityException.class)
    public ResponseEntity<ErrorDTO> catchUnprocessableEntityException(UnprocessableEntityException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(PreconditionFailedException.class)
    public ResponseEntity<ErrorDTO> catchUnprocessableEntityException(PreconditionFailedException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, PRECONDITION_FAILED);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<ErrorDTO> catchUnauthorizedException(UnauthorizedException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, UNAUTHORIZED);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ErrorDTO> catchForbiddenException(ForbiddenException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, FORBIDDEN);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorDTO> catchConstraintViolationException(ConstraintViolationException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDTO> catchNotFoundException(NotFoundException exception) {
        return createResponse(new ErrorDTO().message(exception.getMessage()), exception, NOT_FOUND);
    }

    private ResponseEntity<ErrorDTO> createResponse(final ErrorDTO error, final Throwable throwable, HttpStatus status) {
        LOGGER_ERROR.error(getClass().getName(), throwable);
        return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON).body(error);
    }

}
