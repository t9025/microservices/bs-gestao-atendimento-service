# API Boa Saúde - Gestão Atendimento

### Componentes Projeto

- Projeto desenvolvido em Java 11;
- Maven;
- Spring Framework & Spring Boot;
- Swagger;

### Pré-requisitos

- Java JDK 11 ou superior;
- Apache Maven versão 3.6.0 ou superior;
- IDE Java, em caso de edição do código;
- Postman para execução das collections de testes, também possível executar testes diretamente swagger;

### Instalação e Execução do Projeto
- Clone o projeto;
- Execute o código: `mvn clean install`;
##### Edição do código
- Importar o código fonte na IDE, selecionar a opção importar projeto Maven;
##### Execução do projeto
- Acesse a pasta do projeto e execute o código: `mvn spring-boot:run`;
##### Execução dos testes unitários
- Acesse a pasta do projeto e execute o código: `mvn test`;

### Documentação da API

* A documentação da API gerada pelo Swagger está disponivel após start da aplicação;
